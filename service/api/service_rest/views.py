# from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.utils import timezone
import json
from .models import Technician, Appointment, AutomobileVO
from .encoders import TechnicianEncoder, AppointmentEncoder, AutomobileVOEncoder

timezone.now()


@require_http_methods(["GET", "POST"])
def list_technicians(request):
    """
    When request method is GET, lists technicians' names and employee IDs.

    Returns a dictionary with a single key, "technicians", which is a list
    of technician names and ID numbers.  Each entry in the list is a
    dictionary that contains the technician's first and last name and
    employee ID number.

    {
        "technicians": [
            {
                "first_name": technician's first name
                "last_name": technician's last name
                "employee_id": technician's employee ID number
            }
        ]
    }

    When request method is POST, creates a technician with data from each
    field.  Restrictions: employee_id must be a positive integer less than
    32767.
    """
    if request.method == "GET":
        try:
            techs = Technician.objects.all()
            return JsonResponse(
                {"technicians": techs},
                encoder=TechnicianEncoder
            )
        except:
            response = JsonResponse({"message": "could not display technicians"})
            response.status_code = 400
            return response

    else:  # it's a POST
        try:
            content = json.loads(request.body)
            tech = Technician.objects.create(**content)
            return JsonResponse(
                tech,
                encoder=TechnicianEncoder,
                safe=False
            )
        except:
            response = JsonResponse({"message": "could not create technician"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def show_technician(request, id):
    """
    Shows, updates, or deletes a specific technician based on employee_id.
    """
    if request.method == "GET":
        try:
            tech = Technician.objects.get(id=id)
            return JsonResponse(
                {"tech": tech},
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "technician does not exist"})
            response.status_code = 404
            return response
        except:
            response = JsonResponse({"message": "could not show technician"})
            response.status_code = 400
            return response

    elif request.method == "DELETE":
        try:
            count, _ = Technician.objects.get(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "technician does not exist"})
            response.status_code = 404
            return response
        except:
            response = JsonResponse({"message": "could not delete technician"})
            response.status_code = 400
            return response

    else:  # it's a PUT
        try:
            content = json.loads(request.body)
            Technician.objects.filter(id=id).update(**content)
            tech = Technician.objects.get(id=id)
            return JsonResponse(
                tech,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "technician does not exist"})
            response.status_code = 404
            return response
        except:
            response = JsonResponse({"message": "could not update technician"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def list_appointments(request):
    """
    When request method is GET, lists service appointments.

    Returns a dictionary with a single key, "appointments", which is a list
    of service appointments.  Each entry in the list is a dictionary that
    contains the appointment details.

    {
        "appointments": [
            {
                "date_time": date and time of the appointment
                "reason": reason for the appointment
                "vin": VIN of the automobile to be serviced
                "customer": customer name
                "technician": technician assigned to the appointment
                "status": pending, canceled, or finished
                "id": database ID for the appointment
            }
            ...
        ]
    }

    When request method is POST, creates an appointment with data from
    each field.  Status and ID are auto-created and cannot be submitted
    via POST request.
    """
    if request.method == "GET":
        try:
            appts = Appointment.objects.all()
            return JsonResponse(
                {"appointments": appts},
                encoder=AppointmentEncoder
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "appointment does not exist"})
            response.status_code = 404
            return response
        except:
            response = JsonResponse({"message": "could not show appointments"})
            response.status_code = 400
            return response

    else:  # it's a POST
        try:
            content = json.loads(request.body)
            id = content["technician"]
            tech = Technician.objects.get(id=id)
            content["technician"] = tech
            appt = Appointment.create(**content)
            return JsonResponse(
                appt,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician does not exist"})
            response.status_code = 404
            return response
        except:
            response = JsonResponse({"message": "could not create appointment"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def show_appointment(request, id):
    """
    Shows, updates, or deletes a specific appointment based on the
    Appointment database ID.
    """
    if request.method == "GET":
        try:
            appt = Appointment.objects.get(id=id)
            return JsonResponse(
                {"appointment": appt},
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "appointment does not exist"})
            response.status_code = 404
            return response
        except:
            response = JsonResponse({"message": "could not show appointment"})
            response.status_code = 400
            return response

    elif request.method == "DELETE":
        try:
            count, _ = Appointment.objects.get(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "appointment does not exist"})
            response.status_code = 404
            return response
        except:
            response = JsonResponse({"message": "could not delete appointment"})
            response.status_code = 400
            return response

    else:  # it's a PUT
        try:
            content = json.loads(request.body)
            Appointment.objects.filter(id=id).update(**content)
            appt = Appointment.objects.get(id=id)
            return JsonResponse(
                appt,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "appointment does not exist"})
            response.status_code = 404
            return response
        except:
            response = JsonResponse({"message": "could not update appointment"})
            response.status_code = 400
            return response


@require_http_methods("PUT")
def cancel_appointment(request, id):
    """
    Calls the Appointment method "cancel" on the specific instance of
    Appointment indicated by the database ID passed in.  Updates the status
    of the appointment to "canceled".
    """
    try:
        appt = Appointment.objects.get(id=id)
        appt.cancel()
        return JsonResponse(
            {"appt": appt},
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "appointment does not exist"})
        response.status_code = 404
        return response
    except:
        response = JsonResponse({"message": "could not cancel appointment"})
        response.status_code = 400
        return response


@require_http_methods("PUT")
def finish_appointment(request, id):
    """
    Calls the Appointment method "finish" on the specific instance of
    Appointment indicated by the database ID passed in.  Updates the status
    of the appointment to "finished".
    """
    try:
        appt = Appointment.objects.get(id=id)
        appt.finish()
        return JsonResponse(
            {"appt": appt},
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "appointment does not exist"})
        response.status_code = 404
        return response
    except:
        response = JsonResponse({"message": "could not finish appointment"})
        response.status_code = 400
        return response


@require_http_methods("GET")
def list_inventory(request):
    """
    Lists AutomobileVO objects created by the Service poller.

    Returns a dictionary with a single key, "inventory", which includes
    the vin and sold fields for each Automobile object in Inventory. Each
    entry in the list is a dictionary with "vin" and "sold" fields.

    {
        "inventory": [
            {
                "vin": the VIN for the Automobile in Inventory
                "sold": boolean indicating whether the Automobile in
                    Inventory has been marked as sold
            }
        ]
    }
    """
    try:
        inventory = AutomobileVO.objects.all()
        return JsonResponse(
            {"inventory": inventory},
            encoder=AutomobileVOEncoder,
            safe=False
        )
    except:
        response = JsonResponse({'message': 'could not fetch inventory'})
        response.status_code = 400
        return response
