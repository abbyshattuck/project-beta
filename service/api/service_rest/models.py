from django.db import models


class Technician(models.Model):
    """
    The Technician model holds information for a single technician.
    The first_name and last_name fields can take text up to 200
    characters, while the employee_id field can take a positive
    integer up to 32767.
    """
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.first_name + " " + self.last_name


class AutomobileVO(models.Model):
    """
    The AutomobileVO object holds information about an Automobile
    object in the Inventory microservice.  These VOs are created
    by the poller and include VIN and sold status for each
    Automobile in Inventory.
    """
    vin = models.CharField(max_length=17)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):
    """
    The Appointment model holds information about a specific
    service appointment.  It includes the date and time, reason
    for service, VIN of the automobile to be serviced, name
    of the customer, technician assigned to the appointment,
    and status for the appointment.
    Restrictions:
        - the VIN cannot be more than 17 digits long
        - the technician must be a foreign key to a
            Technician object
        - status can be "pending", "canceled", or "deleted"

    The class method "create" sets the status of a new
    Appointment object to "pending" when it is created.

    The method "cancel" sets the status as "canceled" for
    the Appointment object it is called on.

    The method "finish" sets the status as "finished" for
    the Appointment object it is called on.
    """
    @classmethod
    def create(cls, **kwargs):
        kwargs["status"] = "PENDING"
        appt = cls(**kwargs)
        appt.save()
        return appt

    date_time = models.DateTimeField()
    reason = models.TextField()
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.SET_NULL,
        null=True,
    )
    status = models.CharField(max_length=10)

    def cancel(self):
        status = "canceled"
        self.status = status
        self.save()

    def finish(self):
        status = "finished"
        self.status = status
        self.save()

    def __str__(self):
        return self.customer + " " + self.reason
