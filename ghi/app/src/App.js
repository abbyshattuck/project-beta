import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AutosList from './AutosList';
import AutosForm from './AutosForm';
import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import ModelsList from './ModelsList';
import ModelForm from './ModelForm';
import SalespersonForm from './SalespersonForm';
import SalespeopleList from './ListSalespeople';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalesList from './SalesList';
import TechniciansList from './TechniciansList';
import TechniciansForm from './TechniciansForm';
import AppointmentsList from './AppointmentsList';
import AppointmentsForm from './AppointmentsForm';
import ServiceHistory from './ServiceHistory';
import SaleForm from './SalesForm';
import SalespersonHistory from './SalespersonHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="automobiles">
            <Route index element={<AutosList />} />
            <Route path="new" element={<AutosForm />} />
          </Route>
          <Route path="manufacturers/">
            <Route index element={<ManufacturersList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models/">
            <Route index element={<ModelsList />} />
            <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path="salespeople/">
            <Route index element={<SalespeopleList />} />
            <Route path="new" element={<SalespersonForm />} />
          </Route>
          <Route path="customers/">
            <Route index element={<CustomerList />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="sales/">
            <Route index element={<SalesList />} />
            <Route path="new" element={<SaleForm />} />
            <Route path="history" element={<SalespersonHistory />} />
          </Route>
          <Route path="technicians/">
            <Route index element={<TechniciansList />} />
            <Route path="new" element={<TechniciansForm />} />
          </Route>
          <Route path="appointments/">
            <Route index element={<AppointmentsList />} />
            <Route path="new" element={<AppointmentsForm />} />
          </Route>
          <Route path="servicehistory/">
            <Route index element={<ServiceHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
