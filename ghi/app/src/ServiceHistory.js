import React, { useEffect, useState } from 'react'

function ServiceHistory() {
    let [appts, setAppts] = useState([])
    const getAppts = async () => {
        const response = await fetch("http://localhost:8080/api/appointments")
        if (response.ok) {
            const data = await response.json()
            setAppts(data.appointments)
        }
    }

    const [inventory, setInventory] = useState([])
    const getInventory = async () => {
        const response = await fetch("http://localhost:8080/api/inventory/")
        if (response.ok) {
            const data = await response.json()
            setInventory(data.inventory)
        }
    }
    const vinlist = []
    for (const car of inventory){
        vinlist.push(car.vin)
    }

    const [vin, setVin] = useState('')
    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        let searchedList = []
        for (let appt of appts) {
            if (appt.vin === vin) {
                searchedList.push(appt)
            }
        }
        setAppts(searchedList)
        setVin('')
    }

    useEffect(() => {
        getAppts()
        getInventory()
    }, [])

    return (
        <div className='container'>
            <h2>Service History</h2>
            <div className='container'>
                <form onSubmit={handleSubmit} id="search-by-vin-form">
                    <div className="form-floating mb-3">
                        <input value={vin} onChange={handleVinChange} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                        <label htmlFor="vin">Search by vin...</label>
                        <button className="btn btn-primary">Search</button>
                    </div>
                </form>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Date and time</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Reason</th>
                        <th>Technician</th>
                        <th>VIP?</th>
                    </tr>
                </thead>
                <tbody>
                    {appts.map(appt => {
                        let vip = "no"
                        for (let vin of vinlist) {
                            if (appt["vin"] === vin) {
                                vip = "yes"
                            }
                        }
                        let date = new Date(appt.date_time)
                        let datetime = date.toLocaleString()
                        return (
                            <tr key={appt.id}>
                                <td>{datetime}</td>
                                <td>{appt.customer}</td>
                                <td>{appt.vin}</td>
                                <td>{appt.reason}</td>
                                <td>{appt.technician.employee_id}</td>
                                <td>{vip}</td>
                                <td>{appt.status}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default ServiceHistory
