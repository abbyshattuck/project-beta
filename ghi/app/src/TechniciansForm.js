import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'


function TechniciansForm() {
    const [firstName, setFirstName] = useState('')
    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)
    }

    const [lastName, setLastName] = useState('')
    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }

    const [employeeId, setEmployeeId] = useState('')
    const handleEmployeeIdChange = (event) => {
        const value = event.target.value
        setEmployeeId(value)
    }

    const navigate = useNavigate()

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.first_name = firstName
        data.last_name = lastName
        data.employee_id = employeeId

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const techsUrl = 'http://localhost:8080/api/technicians/'
        const response = await fetch(techsUrl, fetchConfig)

        if (response.ok) {
            setFirstName('')
            setLastName('')
            setEmployeeId('')
            navigate('/technicians/')
        }
    }

    return (
        <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstNameChange} required type="text" id="fname" name="fname" className="form-control" value={firstName} />
                            <label htmlFor="fname">First name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastNameChange} required type="text" id="lname" name="lname" className="form-control" value={lastName} />
                            <label htmlFor="lname">Last name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeIdChange} required type="number" id="employee_id" name="employee_id" className="form-control" value={employeeId} />
                            <label htmlFor="employee_id">Employee ID... (positive integer less than 32767)</label>
                        </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        </div>
    )
}

export default TechniciansForm
