import { useEffect, useState } from 'react';

function ModelsList() {
    const [models, setModels] = useState([])

    const getData = async () => {
      const response = await fetch("http://localhost:8100/api/models/");

      if (response.ok) {
        const data = await response.json();
        setModels(data.models)
      }
  }

    useEffect(()=>{
        getData()
    }, [])


    return (
        <>
        <h1>Models</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Manufacturer</th>
              <th>Picture</th>
            </tr>
          </thead>
          <tbody>
            {models.map(model => {
              return (
                <tr key={model.href}>
                  <td>{ model.name }</td>
                  <td>{ model.manufacturer.name }</td>
                  <td>
                    <img src={model.picture_url} alt="Car Picture" className="img-thumbnail" style={{ width: 100, height: 150 }} />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </>
      );
    }

    export default ModelsList;
