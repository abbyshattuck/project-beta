import { useEffect, useState } from 'react';

function SalespersonHistory() {
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [sales, setSales] = useState([]);
    const [salesperson_sales, SetSalespersonSales] = useState([])


    const getSalespeople = async () => {
        const url = 'http://localhost:8090/api/salespeople/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setSalespeople(data.salespeople);
        }
    }

    const getSales = async () => {
        const url = 'http://localhost:8090/api/sales/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setSales(data.sales);
        }
    }

    useEffect(()=> {
        getSalespeople();
        getSales();
      }, []);

      const handleSalesperson = (event) => {
        const value = event.target.value;
        setSalesperson(value);
      }

      const handleSubmit = async (event) => {
        event.preventDefault();
        let sales_list = []
        for (let sale of sales) {
            if (sale.salesperson.employee_id === salesperson) {
                sales_list.push(sale)
            }
        }
        SetSalespersonSales(sales_list)
        setSalesperson('')

      }


      return (
        <>
        <h1>Salesperson History</h1>
        <form onSubmit={handleSubmit} id="select-salesperson-form">
            <div className="mb-3">
            <select value={salesperson} onChange={handleSalesperson} required name="salesperson" id="salesperson" className="form-select">
                <option value="">Choose a Salesperson</option>
                {salespeople.map(salesperson => {
                    return (
                     <option key={salesperson.employee_id} value={salesperson.employee_id}>
                            {salesperson.first_name} {salesperson.last_name}
                        </option>
                    )
                })}
            </select>
            </div>
            <button className="btn btn-primary">Search</button>
        </form>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Salesperson</th>
              <th>Customer</th>
              <th>Vin</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {salesperson_sales.map(sale => {
              return (
                <tr key={sale.id}>
                  <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                  <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                  <td>{ sale.automobile.vin }</td>
                  <td>{ sale.price }</td>
                </tr>
              );
              })}
          </tbody>
        </table>
        </>
      );
    }

    export default SalespersonHistory;
