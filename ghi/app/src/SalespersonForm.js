import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom'


function SalespersonForm() {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setEmployeeId] = useState('');
    const navigate = useNavigate()


    const fetchData = async () => {
      const url = "http://localhost:8090/api/salespeople/";

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
      }
    }

    useEffect(() => {
      fetchData();
    }, [])

    const handleFirstName = (event) => {
        const value = event.target.value;
        setFirstName(value);
      }

    const handleLastName = (event) => {
        const value = event.target.value;
        setLastName(value);
      }

      const handleEmployeeId = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
      }


      const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id


        const url = "http://localhost:8090/api/salespeople/"

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                }
            };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeId('');
            navigate('/salespeople/')
        }
    }
    return (
        <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Salesperson</h1>
          <form onSubmit={handleSubmit} id="create-salesperson-form">
            <div className="form-floating mb-3">
              <input value={first_name} onChange={handleFirstName} placeholder="first_name" required type="text" name="first_name" id="first_name" className="form-control" />
              <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={last_name} onChange={handleLastName} placeholder="last_name" required type="text" name="last_name" id="last_name" className="form-control" />
              <label htmlFor="last_name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={employee_id} onChange={handleEmployeeId} placeholder="employee_id" required type="text" name="employee_id" id="employee_id" className="form-control" />
              <label htmlFor="employee_id">Employee ID</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>

      );



}

export default SalespersonForm
