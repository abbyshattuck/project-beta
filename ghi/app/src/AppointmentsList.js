import React, { useEffect, useState } from 'react'

function AppointmentsList() {
    const [appts, setAppts] = useState([])
    const getAppts = async () => {
        const response = await fetch("http://localhost:8080/api/appointments")
        if (response.ok) {
            const data = await response.json()
            setAppts(data.appointments)
        }
    }

    const [inventory, setInventory] = useState([])
    const getInventory = async () => {
        const response = await fetch("http://localhost:8080/api/inventory/")
        if (response.ok) {
            const data = await response.json()
            setInventory(data.inventory)
        }
    }
    const vinlist = []
    for (const car of inventory){
        vinlist.push(car.vin)
    }

    const [canceled, setCanceled] = useState('')
    const Cancel = async (id) => {
        const fetchConfig = {
            method: "put",
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const cancelUrl = `http://localhost:8080/api/appointments/${id}/cancel/`
        const response = await fetch(cancelUrl, fetchConfig)
        if (response.ok) {
            const data = await response.json()
            setCanceled(data.appt)
        }
        getAppts()
    }

    const [finished, setFinished] = useState('')
    const Finish = async (id) => {
        const fetchConfig = {
            method: "put",
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const finishUrl = `http://localhost:8080/api/appointments/${id}/finish/`
        const response = await fetch(finishUrl, fetchConfig)
        if (response.ok) {
            const data = await response.json()
            setFinished(data.appt)
        }
        getAppts()
    }

    useEffect(() => {
        getAppts()
        getInventory()
    }, [])

    return (
        <div className='container'>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Date and time</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Reason</th>
                        <th>Technician</th>
                        <th>VIP?</th>
                    </tr>
                </thead>
                <tbody>
                    {appts.map(appt => {
                        let vip = "no"
                        for (let vin of vinlist) {
                            if (appt["vin"] === vin) {
                                vip = "yes"
                            }
                        }
                        let id = appt.id
                        let date = new Date(appt.date_time)
                        let datetime = date.toLocaleString()
                        if (appt.status === "PENDING") {
                            return (
                                <tr key={appt.id}>
                                    <td>{datetime}</td>
                                    <td>{appt.customer}</td>
                                    <td>{appt.vin}</td>
                                    <td>{appt.reason}</td>
                                    <td>{appt.technician.employee_id}</td>
                                    <td>{vip}</td>
                                    <td><button type="button" className="btn btn-outline-danger mx-3" onClick={() => Cancel(id)}>Cancel</button></td>
                                    <td><button type="button" className="btn btn-outline-danger mx-3" onClick={() => Finish(id)}>Finish</button></td>
                                </tr>
                            )
                        }
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default AppointmentsList
