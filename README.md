CarCar:
CarCar is a brilliantly named application used to make the average car dealership function in an organized and efficient way. The application provides the user a way to manage the dealership’s current inventory, automobile sales, and automobile services.


Team:​
* Jenn Trotter - Automobile Services
* Abigail Shattuck - Automobile Sales
​
Setup/Running the application:
*pre-requisites*: Docker, Git, and Node.js 18.2 or above
- Fork Project-Beta from user: abbyshattuck (https://gitlab.com/abbyshattuck/project-beta/)
- Clone forked repository on to your computer (git clone <https://gitlab.com/abbyshattuck/project-beta.git>)
- Create Docker containers (ensure you are in the project directory first):
    docker volume create beta-data
    docker-compose build
    docker-compose up
- Ensure all docker containers are running, this could take several minutes
- Enter “localhost:3000” in your browser to view the home page of the application


::Overview of Microservices::
Our application is made of three microservices: inventory, automobile sales, and automobile services. Our inventory contains the detailed information of all sold and unsold automobiles. Our Automobile Service microservice tracks appointments and technicians, and pulls information from our inventory using a poller, and utilizes that information to ensure all service appointments on vehicles sold through the dealership are given the dealership VIP treatment. Our Automobile Sales microservice utilizes a poller as well to pull updated automobile data from the inventory. It uses this data to create sales for vehicles in the inventory, then removes them from the list of vehicles available for sale.
​
Diagram:
- https://excalidraw.com/#json=PlM-lOuyeSBqO0q3ybCOr,kTTGXtoiGYDzFgNVQF0ilg


URLs and Ports:
Inventory API: Port: 8100 :URL:http://localhost:8100
Automobile Sales API: Port: 8090 : URL: http://localhost:8090
Automobile Service API: Port: 8080 : URL: http://localhost:8080
React: Port: 3000 URL: http://localhost:3000


### Sales API

Explained: The automobile sales service enables the user to view and create current salespeople, customers, and sales. It utilizes a poller to retrieve the most updated list of automobiles and creates a form to create a new sale for all available unsold vehicles.

-Salespeople-
|| action :: method :: url ||
|| List Salespeople :: GET ::  http://localhost:8090/api/salespeople/ ||
|| Create Salesperson :: POST :: http://localhost:8090/api/salespeople/ ||
|| Delete Salesperson :: DELETE :: http://localhost:8090/api/salespeople/employee_id/ ||

Creating a Salesperson JSON Body example:
{
	"first_name": "Tom",
	"last_name": "Sawyer",
	"employee_id": "90289"
}

Create Salesperson Return Example:
{
	"first_name": "Tom",
	"last_name": "Sawyer",
	"employee_id": "90289",
	"id": 5
}

Salesperson List Return Example:
{
	"salespeople": [
		{
			"first_name": "Abigail",
			"last_name": "Shattuck",
			"employee_id": "12345",
			"id": 2
		},
		{
			"first_name": "Erin",
			"last_name": "Davis",
			"employee_id": "90234",
			"id": 3
		},
]
}

	-Customers-
|| action :: method :: url ||
|| List Customers :: GET :: http://localhost:8090/api/customers/ ||
|| Create Customer :: POST :: http://localhost:8090/api/customers/ ||
|| Delete Customer :: DELETE :: http://localhost:8090/api/customers/id/ ||

Creating a Customer JSON Body example:
{
	"first_name": "Jenn",
  	 "last_name": "Blue",
  	 "address": "1765 Apple Tree Lane",
  	 "phone_number": "465-783-9753"
}

Create Customer Return Example:
{
	"first_name": "Jenn",
	"last_name": "Blue",
	"address": "1765 Apple Tree Lane",
	"phone_number": "465-783-9753"
}

Customer List Return Example:
{
	"customers": [
		{
			"first_name": "John",
			"last_name": "Smith",
			"address": "1234 Cherry Lane",
			"phone_number": "123-456-7634",
			"id": 2
		},
		{
			"first_name": "Jane",
			"last_name": "Brown",
			"address": "1765 Lone Tree Road",
			"phone_number": "465-783-9863",
			"id": 3
		}
	]
}

	-Sales-
|| action :: method :: url ||
|| List Sales :: GET :: http://localhost:8090/api/sales/ ||
|| Create Sale:: POST :: http://localhost:8090/api/sales/  ||
|| Delete Sale :: DELETE :: http://localhost:8090/api/sales/1/ ||

Creating a Sale JSON Body example:
{
	"automobile": "1C3CC5FB2AN120174",
	"salesperson": "90234",
	"customer": 4,
	"price": 1200
}

Create Sale Return Example:
{
	"automobile": {
		"vin": "1C3CC5FB2AN120174",
		"sold": true
	},
	"salesperson": {
		"first_name": "Erin",
		"last_name": "Davis",
		"employee_id": "90234",
		"id": 3
	},
	"customer": {
		"first_name": "Jenn",
		"last_name": "Blue",
		"address": "1765 Apple Tree Lane",
		"phone_number": "465-783-9753",
		"id": 4
	},
	"price": 1200,
	"id": 6
}

Sale List Return Example:
{
	"sales": [
		{
			"automobile": {
				"vin": "1C3CC5FB2AN120174",
				"sold": true
			},
			"salesperson": {
				"first_name": "Abigail",
				"last_name": "Shattuck",
				"employee_id": "12345",
				"id": 2
			},
			"customer": {
				"first_name": "John",
				"last_name": "Smith",
				"address": "1234 Cherry Lane",
				"phone_number": "123-456-7634",
				"id": 2
			},
			"price": 4000,
			"id": 13
		}
]
}

​
### Service API

The Service API has three models (Technician, Appointment, and AutomobileVO).  The Appointment model gets data from the Technician model.  The AutomobileVO is a value object created by the Service poller to represent an Automobile object in the Inventory API.  This poller runs every minute, so the Service API can be fully up-to-date.  This allows the Service API to use inventoried car VINs to identify whether a vehicle scheduled for service came from our inventory, in which case the appointment is given VIP status and service.


Use Insomnia to access the following endpoints to send/view data for each of these models.

-.- Technicians -.-

|| action :: method :: url ||
​|| list technicians :: GET :: http://localhost:8080/api/technicians/ ||
|| create a technician :: POST :: http://localhost:8080/api/technicians/ ||
|| delete a technician :: DELETE :: http://localhost:8080/api/technicians/id/ ||

JSON body to create a technician:
Employee ID must be a positive integer less than 32767
{
	“first_name”: “fname”,
	“last_name”: “lname”,
	“employee_id”: 54321
}

Return value of creating a technician:
{
“first_name”: “fname”,
	“last_name”: “lname”,
	“employee_id”: 54321,
	“id”: 1
}

Return value of viewing the technicians list:
{
	“Technicians”: [
		{
			“first_name”: “fname”,
	“last_name”: “lname”,
	“employee_id”: 54321,
	“id”: 1
}
…
	]
}

-.- Appointments -.-

|| action :: method :: url ||
​|| list appointments :: GET :: http://localhost:8080/api/appointments/ ||
|| create an appointment :: POST :: http://localhost:8080/api/appointments/ ||
|| delete an appointment :: DELETE :: http://localhost:8080/api/appointments/id/ ||
|| cancel an appointment :: PUT :: http://localhost:8080/api/appointments/id/cancel/ ||
|| finish an appointment :: PUT :: http://localhost:8080/api/appointments/id/finish/ ||
|| view service history :: GET :: http://localhost:8080/api/servicehistory/ ||


JSON body to create an appointment:
{
	"date_time": "2023-04-20T14:39:00.000Z",
	"vin": "1234567890ABCDEFG”,
	"reason": "reason",
	"customer": "customer name",
	"technician": 2
}

Return value of creating an appointment:
{
	"date_time": "2023-04-20T14:39:00.000Z",
	"reason": "reason code 1",
	"vin": "1234567890ABCDEFG",
	"customer": "customer name",
	"technician": {
		"first_name": "first",
		"last_name": "last",
		"employee_id": 1,
		"id": 2
	},
	"status": "PENDING",
	"id": 2
}

Return value of viewing the appointments list:
{
	“Appointments”: [
		{
	"date_time": "2023-04-20T14:39:00.000Z",
	"reason": "reason code 1",
	"vin": "1234567890ABCDEFG",
	"customer": "customer name",
	"technician": {
		"first_name": "first",
		"last_name": "last",
		"employee_id": 1,
		"id": 2
	},
	"status": "PENDING",
	"id": 2
}
…
	]
}

Return value of canceling an appointment:
{
	"date_time": "2023-04-20T14:39:00.000Z",
	"reason": "reason code 1",
	"vin": "1234567890ABCDEFG",
	"customer": "customer name",
	"technician": {
		"first_name": "first",
		"last_name": "last",
		"employee_id": 1,
		"id": 2
	},
	"status": "canceled",
	"id": 2
}

Return value of finishing an appointment:
{
	"date_time": "2023-04-20T14:39:00.000Z",
	"reason": "reason code 1",
	"vin": "1234567890ABCDEFG",
	"customer": "customer name",
	"technician": {
		"first_name": "first",
		"last_name": "last",
		"employee_id": 1,
		"id": 2
	},
	"status": "finished",
	"id": 2
}


-.- AutomobileVO -.-

|| action :: method :: url ||
​|| list AutomobileVOs :: GET :: http://localhost:8080/api/inventory/ ||

Return value of listing AutomobileVOs:
{
	"inventory": [
		{
			"vin": "1234567890ABCDEFG",
			"sold": false
		},
		…
	]
}

​
## Value Objects

Service: AutomobileVO
- AutomobileVO is a value object created by the Service poller to represent an Automobile object in the Inventory API.  This poller runs every minute, so the Service API can be fully up-to-date.
Sales: AutomobileVO
- AutomobileVO is a value object created by the Sales poller to represent an Automobile object in the Inventory API.  This poller runs every minute, so the Sales API can be fully up-to-date.


### Inventory API

-.- Manufacturers -.-

|| Action :: Method :: URL ||
|| List manufacturers :: GET :: http://localhost:8100/api/manufacturers/ ||
|| Create a manufacturer :: POST :: http://localhost:8100/api/manufacturers/ ||
|| View a manufacturer :: GET :: http://localhost:8100/api/manufacturers/id/ ||
|| Update a manufacturer :: PUT :: http://localhost:8100/api/manufacturers/id/ ||
|| Delete a manufacturer :: DELETE :: http://localhost:8100/api/manufacturers/id/ ||


JSON body to create/update manufacturer:
{
  "name": "Ford"
}

Return value of creating, viewing, updating a single manufacturer:
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Ford"
}

Return list of manufacturers:
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Ford"
    }
  ]
}


-.- Vehicle Models -.-

|| Action :: Method :: URL ||
|| List vehicle models :: GET :: http://localhost:8100/api/models/ ||
|| Create a vehicle model :: POST :: http://localhost:8100/api/models/ ||
|| View a vehicle model :: GET :: http://localhost:8100/api/models/id/ ||
|| Update a vehicle model :: PUT :: http://localhost:8100/api/models/id/ ||
|| Delete a vehicle model :: DELETE :: http://localhost:8100/api/models/id/ ||


Create/Update Model JSON Body:
{
  "name": "Bronco",
  "picture_url": "bronco_image.url"
  "manufacturer_id": 1
}

Updating Vehicle Model can take name and/or picture URL:
{
  "name": "Bronco",
  "picture_url": "new_bronco_img.url"
}

Result of updating a vehicle model:
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Bronco",
  "picture_url": "new_bronco_img.url",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Ford"
  }
}

View list of vehicle models:
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Bronco",
      "picture_url": "new_bronco_img.url",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Ford"
      }
    }
  ]
}


### Automobiles:
- The **'vin'** at the end of the detail urls represents the VIN for the specific automobile you want to access. This is not an integer ID. This is a string value so you can use numbers and/or letters.


|| Action :: Method :: URL ||
|| List automobiles :: GET :: http://localhost:8100/api/automobiles/ ||
|| Create an automobile :: POST :: http://localhost:8100/api/automobiles/ ||
|| View an automobile :: GET :: http://localhost:8100/api/automobiles/vin/ ||
|| Update an automobile :: PUT :: http://localhost:8100/api/automobiles/vin/ ||
|| Delete an automobile :: DELETE :: http://localhost:8100/api/automobiles/vin/ ||


Create an automobile (SEND THIS JSON BODY):
- You cannot make two automobiles with the same vin
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}

Return value of creating an automobile:
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "777",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "R8",
		"picture_url": "image.yourpictureurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Audi"
		}
	}
}

To get the details of a specific automobile, you can query by its VIN:
example url: http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/

Return value of viewing an automobile:
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "green",
  "year": 2011,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "image.yourpictureurl.com",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  }
}

You can update the color and/or year of an automobile (SEND THIS JSON BODY):
{
  "color": "red",
  "year": 2012
}

Return value of viewing a list of automobiles:
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "image.yourpictureurl.com",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      }
    }
  ]
}
